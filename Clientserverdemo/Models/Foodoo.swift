//
//  Foodoo.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

// MARK: - Models

class food_tags: NSObject, NSCoding{
    
    var id: Int
    var name: String
    
    init(_ id: Int,_ name: String) {
        self.id = id
        self.name = name
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.name, forKey: "name")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeInteger(forKey: "id")
        self.name = (aDecoder.decodeObject(forKey: "name") as? String)!
    }
    
    
}

class working_hours : NSObject, NSCoding{
    
    var dayNumber: Int
    var dayName: String
    var open: String
    var close: String
    
    init(_ dayNumber: Int,_ dayName: String,_ open: String,_ close: String) {
        self.dayNumber = dayNumber
        self.dayName = dayName
        self.open = open
        self.close = close
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.dayNumber, forKey: "dayNumber")
        aCoder.encode(self.dayName, forKey: "dayName")
        aCoder.encode(self.open, forKey: "open")
        aCoder.encode(self.close, forKey: "close")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.dayNumber = aDecoder.decodeInteger(forKey: "dayNumber")
        self.dayName = aDecoder.decodeObject(forKey: "dayName") as! String
        self.open = aDecoder.decodeObject(forKey: "open") as! String
        self.close = aDecoder.decodeObject(forKey: "close") as! String
    }
}
class photo : NSObject, NSCoding {
    var url : String
    
    init(_ url: String) {
        self.url = url
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.url, forKey: "url")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.url = aDecoder.decodeObject(forKey: "url") as! String
    }
}
class restaurant_cords : NSObject, NSCoding {
    
    
    var lat : Float?
    var lon : Float?
    
    init(_ lat: Float,_ lon: Float) {
        self.lat = lat
        self.lon = lon
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.lat, forKey: "lat")
        aCoder.encode(self.lon, forKey: "lon")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.lat = aDecoder.decodeObject(forKey: "lat") as? Float
        self.lat = aDecoder.decodeObject(forKey: "lon") as? Float
    }
}

struct FoodooModel {
    
    var id: Int?
    var restaurant_name: String?
    var subtitle: String?
    var price_rate: Int?
    var time_walk: String?
    var time_car: String?
    var restaurant_image : String?
    var restaurant_address : String?
    var phone : String?
    var website : String?
    var is_open : Int?
    var status: String?
    var minutes_to_close: Int?
    var internal_rate: Int?
    var internal_rate_dynamic: Int?
    var internal_rate_approval: Int?
    var internal_rate_count_agree : Int?
    var zagat_rate : Int?
    var yelp_rate : Int?
    var tripadviser_rate : Int?
    var zomato_rate : Int?
    var michelin : Int?
    var food_tags: [food_tags]?
    var extra_tags: [food_tags]?
    var place_tags: [food_tags]?
    var working_hours: [working_hours]?
    var photos: [photo]?
    var restaurant_cords: restaurant_cords?
    var booking : String?
    var bookmark : Int?
    var explore_menu : String?
    var distance : String?
    var modified_date : String?
    var restimage : UIImage?
    var photosimage : [UIImage]?
    var workTime : [String]!
    
}
struct FoodooView {
    
    var  data : Foodoo!
    
    var  heightCells =  NSMutableArray()
    var  listCells = NSMutableArray()
    var  allCells =  NSMutableArray()
    
    init () {
        
        self.allCells.addObjects(from:([
            
            //0
            ["type":"",
             "title" : "",
             "instype" : "",
             "insert" : false],
            
            //1
            ["type":"",
             "title" : "",
             "instype" : "",
             "insert" : false],
            
            //2
            ["type":"",
             "title" : MainTitles.TimeWork.rawValue,
             "instype" : CellType.TimeWorkCell.rawValue,
             "insert" : false],
            
            //3 Insert
            ["type":CellType.TimeWorkCell.rawValue,
             "title" : "",
             "instype" : CellType.TimeWorkCell.rawValue,
             "insert" : false],
            
            //4
            ["type":"",
             "title" : "",
             "instype" : CellType.OrderCell.rawValue,
             "insert" : false],
            
            //5
            ["type":"",
             "title" : MainTitles.Order.rawValue,
             "instype" : CellType.OrderCell.rawValue,
             "insert" : false],
            
            //6 Insert
            ["type":CellType.OrderCell.rawValue,
             "title" : "",
             "instype" : CellType.OrderCell.rawValue,
             "insert" : false],
            
            //7
            ["type":"",
             "title" : "",
             "instype" : CellType.FbCell.rawValue,
             "insert" : false],
            
            //8
            ["type":"",
             "title" : MainTitles.Menu.rawValue,
             "instype" : CellType.FbCell.rawValue,
             "insert" : false],
            
            //9
            ["type":"",
             "title" : MainTitles.Facebook.rawValue,
             "instype" : CellType.FbCell.rawValue,
             "insert" : false],
            
            //10 Insert
            ["type":CellType.FbCell.rawValue,
             "title" : "",
             "instype" : CellType.FbCell.rawValue,
             "insert" : false],
            
            //11
            ["type":"",
             "title" : MainTitles.Twitter.rawValue,
             "instype" : "",
             "insert" : false],
            
            //12
            ["type":"",
             "title" : MainTitles.Instagram.rawValue,
             "instype" : "",
             "insert" : false],
            
            //13
            ["type":"",
             "title" : MainTitles.Website.rawValue,
             "instype" : "",
             "insert" : false],
            
            //14
            ["type":"",
             "title" : MainTitles.Suggestions.rawValue,
             "instype" : "",
             "insert" : false],
            ]))
        
        self.heightCells.addObjects(from:([
            //0
            Float(125.0),
            //1
            Float(50.0),
            //2
            Float(50.0),
            //3
            Float(112.0),
            //4
            Float(50.0),
            //5
            Float(190.0),
            //6
            Float(50.0),
            //7
            Float(50.0),
            //8
            Float(50.0),
            //9
            Float(50.0),
            //10
            Float(50.0),
            //11
            Float(50.0)
            ]))
        
        
        self.listCells.addObjects(from:([
            
            //0
            ["type" : CellType.TagCell.rawValue,
             "title": MainTitles.Order.rawValue
            ],
            
            //1
            ["type" : CellType.PlaceCell.rawValue,
             "title" : ""
            ],
            
            //2
            ["type" : CellType.MainCell.rawValue,
             "title" : MainTitles.TimeWork.rawValue,
             "image" : UIImage.init(named: "arrowDown") as Any
            ],
            
            //3
            ["type" : CellType.PhotosCell.rawValue,
             "title" : ""
            ],
            
            //4
            ["type" : CellType.MainCell.rawValue,
             "title" : MainTitles.Order.rawValue,
             "image" : UIImage.init(named: "arrowDown") as Any
            ],
            
            //5
            ["type" : CellType.LeagueCell.rawValue,
             "title" : ""
            ],
            
            //6
            ["type" : CellType.MainCell.rawValue,
             "title": MainTitles.Menu.rawValue,
             "image" : UIImage.init(named: "arrowDown") as Any
            ],
            
            //7
            ["type" : CellType.MainCell.rawValue,
             "title" : MainTitles.Facebook.rawValue,
             "image" : UIImage.init(named: "arrowDown") as Any
            ],
            
            //8
            ["type" : CellType.MainCell.rawValue,
             "title" : MainTitles.Twitter.rawValue,
             "image" : UIImage.init(named: "arrowDown") as Any
            ],
            
            //9
            ["type" : CellType.MainCell.rawValue,
             "title" : MainTitles.Instagram.rawValue,
             "image" : UIImage.init(named: "arrowDown") as Any
            ],
            
            //10
            ["type" : CellType.MainCell.rawValue,
             "title" : MainTitles.Website.rawValue,
             "image" : UIImage.init(named: "arrowRight") as Any
            ],
            
            //11
            ["type" : CellType.MainCell.rawValue,
             "title" : MainTitles.Suggestions.rawValue,
             "image" : UIImage.init(named: "arrowRight") as Any
            ]
            ]))
    }
}
struct Tags {
    
    var data : [[Any]]
    
    init(_ param : Foodoo) {
        data = [param.model.food_tags!,param.model.extra_tags!,param.model.place_tags!];
    }
}

// MARK: - Foodoo

class Foodoo: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(model.food_tags, forKey: "food_tags")
    }
    
    required init?(coder aDecoder: NSCoder) {
        model.id = aDecoder.decodeInteger(forKey:"id")
    }
    

    var model : FoodooModel!
    
    override init() {
        model = FoodooModel()
    }
    init(_ param: [String : Any]) {
        
        super.init()
        
        model = FoodooModel()

        model.id = param["id"] as? Int
        model.restaurant_name = param["restaurant_name"] as? String
        model.subtitle = param["subtitle"] as? String
        model.price_rate = param["price_rate"] as? Int
        model.time_walk = param["time_walk"] as? String
        model.time_car = param["time_car"] as? String
        model.restaurant_image = param["restaurant_image"] as? String
        model.restaurant_address = param["restaurant_address"] as? String
        model.phone = param["phone"] as? String
        model.website = param["website"] as? String
        model.is_open = param["is_open"] as? Int
        model.status = param["status"] as? String
        model.minutes_to_close = param["minutes_to_close"] as? Int
        model.internal_rate = param["internal_rate"] as? Int
        model.internal_rate_dynamic = param["internal_rate_dynamic"] as? Int
        model.internal_rate_approval = param["internal_rate_approval"] as? Int
        model.internal_rate_count_agree = param["internal_rate_count_agree"] as? Int
        model.zagat_rate = param["zagat_rate"] as? Int
        model.yelp_rate = param["yelp_rate"] as? Int
        model.tripadviser_rate = param["tripadviser_rate"] as? Int
        model.zomato_rate = param["zomato_rate"] as? Int
        model.michelin = param["michelin"] as? Int
        model.booking = param["booking"] as? String
        model.bookmark = param["bookmark"] as? Int
        model.explore_menu = param["explore_menu"] as? String
        model.distance = param["explore_menu"] as? String
        model.modified_date = param["explore_menu"] as? String
        model.food_tags = foodTags((param["food_tags"] as! [[String : Any]]))
        model.extra_tags = foodTags((param["extra_tags"] as! [[String : Any]]))
        model.place_tags = foodTags((param["place_tags"] as! [[String : Any]]))
        model.working_hours = workingHours((param["working_hours"] as! [[String : Any]]))
        model.photos = photos((param["photos"] as! [[String : Any]]))
        model.restaurant_cords = restaurantCords ((param["restaurant_cords"] as![String : Any]))
        model.workTime = self.formatTimeWork(param["working_hours"] as! [[String : Any]])
        
    }

    fileprivate func foodTags(_ param : [[String : Any]]) -> [food_tags]{
        
        let temp = NSMutableArray ()
        for obj in param {
            temp.add(food_tags(obj["id"] as! Int,obj["name"] as! String))
        }
        return (temp as? [food_tags])!
    }
    
    fileprivate func workingHours(_ param : [[String : Any]]) -> [working_hours]{
        let temp = NSMutableArray ()
        for obj in param {
            temp.add(working_hours(obj["dayNumber"] as! Int,obj["dayName"]  as! String, obj["open"] as! String,obj["close"] as! String))
        }
        return (temp as? [working_hours])!
    }
    
    fileprivate func photos(_ param : [[String : Any]]) -> [photo]{
        let temp = NSMutableArray ()
        for obj in param {
            temp.add(photo((obj["url"] as? String)!))
        }
        return (temp as? [photo])!
    }
    
    fileprivate func restaurantCords(_ param : [String : Any])  -> restaurant_cords{
        
        return restaurant_cords((param["lat"] as? Float)!, (param["lon"] as? Float)!)
    }
}
