//
//  DFFoodooController.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

// MARK: - Enums

enum MainTitles : String {
    
    case TimeWork = "Time Work"
    case Order = "Must order"
    case League = "Foodoo League"
    case Menu = "Explore the menu"
    case Facebook = "Facebook"
    case Twitter = "Twitter"
    case Instagram = "Instagram"
    case Website = "Website"
    case Suggestions = "Suggestions"
    
    static let allValues = [Order.rawValue, League.rawValue, Menu.rawValue, Facebook.rawValue, Twitter.rawValue, Instagram.rawValue, Website.rawValue, Suggestions.rawValue]
}

enum CellType : String {
    
    case MainCell = "MainCell"
    case TagCell = "TagCell"
    case PlaceCell = "PlaceCell"
    case TimeWorkCell = "TimeWorkCell"
    case PhotosCell = "PhotosCell"
    case OrderCell = "OrderCell"
    case LeagueCell = "LeagueCell"
    case FbCell = "FbCell"
    
    static let allValues = [MainCell.rawValue, TagCell.rawValue, PlaceCell.rawValue, TimeWorkCell.rawValue, PhotosCell.rawValue, OrderCell.rawValue, LeagueCell.rawValue, FbCell.rawValue]
}
enum CellSizeType : Float {
    case TimeWorkSize = 245.0
    case OrderSize = 150.0
    case FBSize = 495.0
    
    static let allValues = [TimeWorkSize.rawValue, OrderSize.rawValue, FBSize.rawValue]
}

// MARK: - FoodooController

class FoodooController : UIViewController {

    @IBOutlet weak var table : UITableView!
    @IBOutlet weak var downView : UIView!
    @IBOutlet weak var callBut : UIButton!
    @IBOutlet weak var bookBut : UIButton!
 
    
    private var  content : FoodooView!
    private var  headerView : HeaderView!
    private var  isDownShow : Bool!
    private var  isTableFrame : Bool!
    private var  insertCount : Bool! = false

    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        prepareController()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Coordinator.sharedInstance.attachView(self)
        checkContent()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Coordinator.sharedInstance.detachView()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if(self.headerView == nil){
            headerView = (Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)![0] as? HeaderView)!
            headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + UIApplication.shared.statusBarFrame.height)
        }
        self.table.contentInset = UIEdgeInsets.zero
        self.table.setContentOffset(CGPoint.init(x: 0, y: UIApplication.shared.statusBarFrame.height), animated: false)
        
    }
    
    //MARK: - Private methods
    
    fileprivate func animeDownView(_ state : Bool) {
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            let frameDW = state ? CGRect.init(x: 0, y: self.view.bounds.height - self.downView.frame.height, width: self.view.bounds.width, height: self.downView.frame.height) : CGRect.init(x: 0, y: self.view.bounds.height + self.downView.frame.height, width: self.view.bounds.width, height: self.downView.frame.height)
            self.downView.frame = frameDW
        }, completion: { finished in
            self.isDownShow = state
        })
    }

    fileprivate func prepareController() {

        self.content = FoodooView()

        isDownShow = false
        isTableFrame = true
        
        self.table.frame = CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        self.downView.frame = CGRect.init(x: 0, y: self.view.bounds.height + 50.0, width: self.view.bounds.width, height: 50.0)
        self.callBut.frame = CGRect.init(x: 0, y: 0.0, width: self.view.bounds.width / 2, height: self.downView.bounds.height)
        self.bookBut.frame = CGRect.init(x: self.view.bounds.width / 2, y: 0, width: self.view.bounds.width / 2, height: self.downView.bounds.height)
        
        self.table.tableFooterView = UIView.init(frame: CGRect.zero)
        self.table.isHidden = true
 
        self.view.addConstraint(NSLayoutConstraint(item: table,attribute: NSLayoutAttribute.top,relatedBy: NSLayoutRelation.equal,toItem: self.view,
                                                    attribute: NSLayoutAttribute.top,multiplier: 1.0, constant: 0))
        
        registeredCells()

    }
 
    fileprivate func registeredCells() {
        for obj in CellType.allValues {
            self.table.register(UINib(nibName: obj as String, bundle: nil), forCellReuseIdentifier: obj as String)
        }
    }
    
    fileprivate func checkContent() {
 
        DataManager.sharedInstance.getItem {(_ result: Foodoo?) in
            DispatchQueue.main.async {
                if result != nil{
           
                    self.updateContent(result!)
                } else {
                    if self.isInternetAvailable() {
                        Coordinator.sharedInstance.foodooContent(false)
                    }
                }
                
            }
        }
    }
}

//MARK: - UITableView dataSource

extension FoodooController: UITableViewDataSource {
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.listCells.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var cell : UITableViewCell? = nil
        
        
        let obj = content.listCells [indexPath.row] as! [String : Any]
        
        
        let cellReuseIdentifier = obj["type"] as! String
        
        
        if cellReuseIdentifier == CellType.MainCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MainCell
            
            Cell.content = obj
            cell = Cell
        } else  if cellReuseIdentifier == CellType.TagCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! TagCell
            Cell.content = content.data
            cell = Cell
        } else if cellReuseIdentifier == CellType.PlaceCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! PlaceCell
            Cell.content = content.data
            cell = Cell
        } else if cellReuseIdentifier == CellType.PlaceCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! PlaceCell
            Cell.content = content.data
            cell = Cell
        } else if cellReuseIdentifier == CellType.TimeWorkCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! TimeWorkCell
            Cell.content = content.data
            cell = Cell
        } else if cellReuseIdentifier == CellType.PhotosCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! PhotosCell
            Cell.content = content.data
            cell = Cell
        } else if cellReuseIdentifier == CellType.OrderCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! OrderCell
            cell = Cell
        } else if cellReuseIdentifier == CellType.LeagueCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! LeagueCell
            cell = Cell
        } else if cellReuseIdentifier == CellType.FbCell.rawValue {
            let Cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! FbCell
            cell = Cell
        }
        
        
        return cell!
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return CGFloat(content.heightCells[indexPath.row] as! Float)
    }
    
    
    
    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.layoutMargins = UIEdgeInsets.zero
        cell.separatorInset = UIEdgeInsets.zero
        
    }
}

//MARK: - UITableView delegate

extension FoodooController: UITableViewDelegate {
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let indPath = indexPath.row + 1
        let objlist = content.listCells [indexPath.row] as! [String : Any]
        let obj = objlist["type"] as! String
        
        if obj == CellType.OrderCell.rawValue || obj == CellType.TimeWorkCell.rawValue || obj == CellType.FbCell.rawValue || obj == CellType.TagCell.rawValue  || obj == CellType.PhotosCell.rawValue || obj ==  CellType.PlaceCell.rawValue || obj == CellType.LeagueCell.rawValue {
            return
        }
        if obj == CellType.MainCell.rawValue {
            let cell = tableView.cellForRow(at: indexPath) as! MainCell
            let param = objlist["title"] as! String
            if  param == MainTitles.Twitter.rawValue || param == MainTitles.Instagram.rawValue || param == MainTitles.Suggestions.rawValue{
                return
            }
            if param == MainTitles.Website.rawValue || param == MainTitles.Menu.rawValue {
                cell.foodooContent = content.data
                return
            }
            cell.rotate()
        }
        
        var preins = content.allCells[indexPath.row] as! [String : Any]
        var ins = content.allCells[indPath] as! [String : Any]
        let type = preins["instype"] as! String
        if type == CellType.OrderCell.rawValue || type == CellType.TimeWorkCell.rawValue || type == CellType.FbCell.rawValue {
            let insert = ins["insert"] as! Bool
            if(insert) {
                content.listCells.removeObject(at:indPath)
                content.heightCells.removeObject(at:indPath)
                tableView.beginUpdates()
                tableView.deleteRows(at: [IndexPath(row: indPath, section: 0)], with: .fade)
                tableView.endUpdates()
            } else {
                content.listCells.insert(["type" : preins["instype"] as! String,
                                          "title" : ""], at: indPath)
                if type == CellType.TimeWorkCell.rawValue {
                    content.heightCells.insert(Float(CellSizeType.allValues[0]), at: indPath)
                } else  if type == CellType.OrderCell.rawValue {
                    content.heightCells.insert(Float(CellSizeType.allValues[1]), at: indPath)
                } else  if type == CellType.FbCell.rawValue {
                    content.heightCells.insert(Float(CellSizeType.allValues[2]), at: indPath)
                }
                
                tableView.beginUpdates()
                tableView.insertRows(at: [IndexPath(row: indPath, section: 0)], with: .fade)
                tableView.endUpdates()
            }
            
            if type == CellType.TimeWorkCell.rawValue {
                var rep = content.allCells[indexPath.row] as! [String : Any]
                rep["insert"] = !insert
                content.allCells.replaceObject(at: indexPath.row, with: rep)
                content.allCells.replaceObject(at: indPath, with: rep)
            } else  if type == CellType.OrderCell.rawValue {
                var rep = content.allCells[indexPath.row] as! [String : Any]
                rep["insert"] = !insert
                content.allCells.replaceObject(at: indexPath.row, with: rep)
                content.allCells.replaceObject(at: indPath, with: rep)
            } else  if type == CellType.FbCell.rawValue {
                var rep = content.allCells[indexPath.row] as! [String : Any]
                rep["insert"] = !insert
                content.allCells.replaceObject(at: indexPath.row, with: rep)
                content.allCells.replaceObject(at: indPath, with: rep)
                content.allCells.replaceObject(at: indPath + 1, with: rep)
            }
        }
    }
}

//MARK: - UIScrollView delegate

extension FoodooController: UIScrollViewDelegate {
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.table {
            
            let contentOffset = scrollView.contentOffset.y
            
            if contentOffset <= (UIApplication.shared.statusBarFrame.height * -1) {
                self.table.setContentOffset(CGPoint.init(x: 0, y: UIApplication.shared.statusBarFrame.height), animated: true)
            }
            if contentOffset >=  50  {
                if !isDownShow{
                    self.animeDownView(!isDownShow)
                }
            } else if contentOffset <  70 {
                if isDownShow{
                    self.animeDownView(!isDownShow)
                }
            }
            
        }
    }
}
//MARK: - Cooordinator protocol

extension FoodooController: CooordinatorDelegate{
    
    func updateContent(_ obj:  Any) {
        content.data = obj as! Foodoo
        DispatchQueue.main.async {
            self.headerView.content = self.content.data
            self.table.tableHeaderView = self.headerView
            self.table.isHidden = false
            self.table .reloadData()
        }
    }
}

