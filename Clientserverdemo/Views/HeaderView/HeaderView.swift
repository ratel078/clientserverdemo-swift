//
//  HeaderView.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import Toucan
import SDWebImage

class HeaderView: UIView {

    @IBOutlet weak var closeView : UIImageView!
    @IBOutlet weak var shareView : UIImageView!
    @IBOutlet weak var bookmarkView : UIImageView!
    @IBOutlet var countViews:[UILabel]!
    @IBOutlet weak var logoView : UIImageView!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var subtitle : UILabel!
    
    fileprivate func loadImages() {
        let manager:SDWebImageManager = SDWebImageManager.shared()
        let requestURL:NSURL = URL(string:(content?.model.restaurant_image)!)! as NSURL
        
        manager.loadImage(with: requestURL as URL, options: SDWebImageOptions.highPriority, progress: { (start, progress, url) in
            
        }) { (image, data, error, cached, finished, url) in
            if (error == nil && (image != nil) && finished) {
                DispatchQueue.main.async {
                    self.logoView?.image =  Toucan(image: image!).resize(self.frame.size,  fitMode: Toucan.Resize.FitMode.crop).image
                }
            }
        }
    }
    
    var content : Foodoo! {
        didSet  {
            if content?.model.restimage != nil{
                let image =  content?.model.restimage
                self.logoView?.image =  Toucan(image: image!).resize(UIScreen.main.bounds.size,  fitMode: Toucan.Resize.FitMode.crop).image
            } else {
                loadImages()
            }
            
          
            self.name.text = content?.model.restaurant_name
            let param = content?.model.subtitle
            let subs = param?.components(separatedBy: ",")
            self.subtitle.text = subs?[0]
            self.countViews[0].text =  content.model.zagat_rate != nil  ? "\(content.model.zagat_rate!)" : "0"
            self.countViews[1].text =  content.model.yelp_rate != nil  ? "\(content.model.yelp_rate!)" : "0"
            self.countViews[2].text = content.model.tripadviser_rate != nil  ? "\(content.model.tripadviser_rate!)" : "0"
            self.countViews[3].text = content.model.zomato_rate != nil  ?"\(content.model.zomato_rate!)" : "0"
        
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.closeView.tintColor = UIColor.white
        self.shareView.tintColor = UIColor.white
        self.bookmarkView.tintColor = UIColor.white
    }

    
}
