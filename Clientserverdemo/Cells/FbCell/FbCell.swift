//
//  FbCell.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 08.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class FbCell: UITableViewCell {

    @IBOutlet weak var webviewInstance : UIWebView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        webviewInstance.isOpaque = false
        webviewInstance.backgroundColor = UIColor.clear
        let requestURL:NSURL = URL(string:"http://facebook.com")! as NSURL
        let request = URLRequest(url: requestURL as URL)
        webviewInstance.loadRequest(request)
    }

}
