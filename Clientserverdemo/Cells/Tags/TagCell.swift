//
//  TagCell.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class TagCell: UITableViewCell {

   @IBOutlet var tagViews:[UIButton]!
   @IBOutlet var titleViews:[UIImageView]!
   @IBOutlet var subtagViews:[UIButton]!
    
    var content : Foodoo? {
        didSet{
            if content != nil {
                if let food_tags = content?.model.food_tags {
                    self .createTags(food_tags)
                    self.clicked(sender: tagViews[0])
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        for i in 0..<self.tagViews.count {
            self.tagViews[i].tag = i
        }
        for i in 0..<self.subtagViews.count {
            self.subtagViews[i].isHidden = true
        }
    }

    @IBAction func clicked(sender: UIButton){
       
        self.clearButtons()
        self.titleViews[sender.tag].tintColor = UIColor(red: 98.0 / 255.0, green: 53.0 / 255.0, blue: 114.0 / 255.0, alpha: 1.0)
        self.tagViews[sender.tag].isSelected = true
        var param : [food_tags] = (content?.model.food_tags)!
        switch sender.tag {
        case 0:
            param = (content?.model.food_tags)!
            break
        case 1:
            param = (content?.model.place_tags)!
            break
        case 2:
            param = (content?.model.extra_tags)!
            break
        default:
           
            break
        }
        self .createTags(param)
    }
    
    
    fileprivate func clearButtons() {
        for obj in self.titleViews {
            obj.tintColor = UIColor(red: 177.0 / 255.0, green: 177.0 / 255.0, blue: 177.0 / 255.0, alpha: 1.0)
        }
        for obj in self.tagViews {
            obj.isSelected = false
        }
        for obj in self.subtagViews {
            obj.isHidden = true
        }
    }
    
    fileprivate func createTags(_ param : [food_tags]?) {
        
        if( param == nil) {
            return
        }
        for i in 0..<param!.count {
            if(i < self.subtagViews.count){
                print(param![i].name)
                self.subtagViews[i].setTitle(param![i].name, for: UIControlState.normal)
                self.subtagViews[i].setTitle(param![i].name, for: UIControlState.selected)
                if param![i].name != "" {
                    self.subtagViews[i].isHidden = false
                }
            }
        }
    }
}
