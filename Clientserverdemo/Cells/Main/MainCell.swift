//
//  MainCell.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 07.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class MainCell: UITableViewCell {

    @IBOutlet weak var titleView : UILabel!
    @IBOutlet weak var accessView : UIImageView!
    
    var isSelect : Bool = false
    
    var foodooContent : Foodoo? {
        didSet{
            if foodooContent != nil {
                 if let title = self.content?["title"] as? String {
                    if(title == MainTitles.Website.rawValue){
                        openUrl(foodooContent?.model.website)
                    } else if(title == MainTitles.Menu.rawValue){
                        openUrl(foodooContent?.model.restaurant_image)
                    }
                }
            }
        }
    }
    
    var content : [String : Any]? {
        didSet{
            titleView.text = nil
            accessView.image = nil
            if content != nil {
                if let title = content?["title"] as? String {
                    titleView.text = title
                }
                if let image = content?["image"] as? UIImage {
                    accessView.image = image
                }
            }
        }
    }
    
    
   func rotate(){
    
        if(self.accessView.image != nil){
            if isSelect {
                self.accessView.image = self.rotateImage(image: self.accessView.image! , angle: CGFloat(Double.pi), flipVertical: 0, flipHorizontal: 0)
            } else {
                self.accessView.image = self.rotateImage(image: self.accessView.image! , angle: (CGFloat(Double.pi * -1)), flipVertical: 0, flipHorizontal: 0)
            }
            isSelect = !isSelect
        }
    
    
    }

    fileprivate func openUrl(_ obj: String?) {
        if obj != nil {
            guard let url = URL(string: obj!) else {
                return
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
