//
//  LeagueCell.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 08.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class LeagueCell: UITableViewCell {

    @IBOutlet var scrollView : UIScrollView!
    var list = NSMutableArray()

    fileprivate func cerateContent() {
        var pos  = 0.0
        for _ in 0...5 {
            list.add(UIImage.init(named: "group_3") ?? UIImage())
            list.add(UIImage.init(named: "group_4") ?? UIImage())
            list.add(UIImage.init(named: "group_5") ?? UIImage())
            list.add(UIImage.init(named: "group_6") ?? UIImage())
        }
        for obj in list {
            scrollView.addSubview(self.imageCell(photo(""), CGRect.init(x: pos, y: 0.0, width: 110.0, height: Double(scrollView.frame.height)),obj as? UIImage))
            pos = pos + 110.0
        }
        scrollView.contentSize = CGSize.init(width: pos, height: Double(scrollView.frame.height))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cerateContent()
    }

  
    
}
