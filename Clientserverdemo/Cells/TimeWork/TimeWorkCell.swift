//
//  TimeWorkCell.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 08.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class TimeWorkCell: UITableViewCell {

    @IBOutlet var timeViews:[UILabel]!

    
    var content : Foodoo? {
        didSet{
            if content != nil {
                if let workTime = content?.model.workTime {
                    let start = 3
                    let startContwnt = workTime.count / 2
                    for i in 0...startContwnt {
                        timeViews[start - i].text = workTime[startContwnt - i]
                    }
                    for i in start..<workTime.count {
                        timeViews[i + 1].text = workTime[i]
                    }
                }
            }
        }
    }

}
