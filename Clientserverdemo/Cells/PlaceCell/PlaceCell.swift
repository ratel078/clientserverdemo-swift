//
//  PlaceCellCell.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 08.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class PlaceCell: UITableViewCell {

     @IBOutlet var title:UILabel!
     @IBOutlet var mapBut:UIImageView!
     @IBOutlet var disBut:UIButton!
    
    var content : Foodoo? {
        didSet{
            title.text = nil
            if let text = content?.model.restaurant_address{
                title.text = text
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mapBut.tintColor = UIColor(red: 177.0 / 255.0, green: 177.0 / 255.0, blue: 177.0 / 255.0, alpha: 1.0)
    }


    @IBAction func clicked(sender: UIButton){
        self.openMapForPlace()
    }
    func openMapForPlace() {
  
        if content?.model.restaurant_cords?.lat == nil || content?.model.restaurant_cords?.lon == nil {
            return
        }
        let latitude: CLLocationDegrees = CLLocationDegrees(Float((content?.model.restaurant_cords!.lat)!))
        let longitude: CLLocationDegrees = CLLocationDegrees(Float((content?.model.restaurant_cords!.lon)!))
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = content?.model.restaurant_name!
        mapItem.openInMaps(launchOptions: options)
    }
    
}
