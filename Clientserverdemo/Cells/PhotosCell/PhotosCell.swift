//
//  PhotosCellCell.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 08.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import SDWebImage
import Toucan

class PhotosCell: UITableViewCell {

    @IBOutlet var scrollView : UIScrollView!
    var listContent : [Any]!

    var content : Foodoo? {
        didSet{
            if content != nil {
                if content?.model.photosimage != nil {
                    self.listContent = (content?.model.photosimage)! as [UIImage]
                } else {
                    self.listContent = (content?.model.photos)! as [photo]
                }
                cerateContent()
            }
            
        }
    }
    
    fileprivate func cerateContent() {
        var pos  = 0.0
        let addBut : UIButton = UIButton.init(type: UIButtonType.custom)
        addBut.frame = CGRect.init(origin: CGPoint.init(x: pos, y: 0), size: CGSize.init(width: 60.0,height: scrollView.frame.height))
        addBut.setImage(UIImage.init(named: "stackedGroup_9"), for: UIControlState.normal)
        scrollView.addSubview(addBut)
        pos = pos + 60.0
        
        for obj in self.listContent {
            if(obj is UIImage){
                scrollView.addSubview(self.imageCell(photo(""), CGRect.init(x: pos, y: 0.0, width: 160.0, height: Double(scrollView.frame.height)),obj as? UIImage))
            } else {
                scrollView.addSubview(self.imageCell(obj as? photo, CGRect.init(x: pos, y: 0.0, width: 160.0, height: Double(scrollView.frame.height)),UIImage()))
            }
            pos = pos + 160.0
        }
        scrollView.contentSize = CGSize.init(width: pos, height: Double(scrollView.frame.height))
    }

}
