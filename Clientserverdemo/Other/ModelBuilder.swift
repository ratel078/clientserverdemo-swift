//
//  modelBuilder.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

class modelBuilder: NSObject {

    func createFoodoo(_ param : Any) -> Foodoo {
        return Foodoo(param as! [String : Any])
    }
}
