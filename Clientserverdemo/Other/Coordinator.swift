//
//  Coordinator.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

protocol CooordinatorDelegate : NSObjectProtocol {
    func updateContent(_ content : Any)
}

class Coordinator: NSObject {

    static let sharedInstance = Coordinator()
    weak var foodooVuew: CooordinatorDelegate?
    
    func attachView(_ view:CooordinatorDelegate){
        foodooVuew = view
    }
    
    func detachView() {
        foodooVuew = nil
    }

    func foodooContent(_ isUpdate : Bool){
        DataManager.sharedInstance.foodooContent {(_ result: Any) in
            let builder = modelBuilder()
            let obj = builder.createFoodoo(result) as Foodoo
            DataManager.sharedInstance.saveItem(obj)
            if !isUpdate {
                self.foodooVuew?.updateContent(obj)
            }
        }
    }

}
