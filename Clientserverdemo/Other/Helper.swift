//
//  Helper.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import SystemConfiguration
import SDWebImage
import Toucan

// MARK: - Extension

extension NSObject {
 
    public func rotateImage(image:UIImage, angle:CGFloat, flipVertical:CGFloat, flipHorizontal:CGFloat) -> UIImage? {
        let ciImage = CIImage(image: image)
        
        let filter = CIFilter(name: "CIAffineTransform")
        filter?.setValue(ciImage, forKey: kCIInputImageKey)
        filter?.setDefaults()
        
        let newAngle = angle * CGFloat(-1)
        
        var transform = CATransform3DIdentity
        transform = CATransform3DRotate(transform, CGFloat(newAngle), 0, 0, 1)
        transform = CATransform3DRotate(transform, CGFloat(Double(flipVertical) * Double.pi), 0, 1, 0)
        transform = CATransform3DRotate(transform, CGFloat(Double(flipHorizontal) * Double.pi), 1, 0, 0)
        
        let affineTransform = CATransform3DGetAffineTransform(transform)
        
        filter?.setValue(NSValue(cgAffineTransform: affineTransform), forKey: "inputTransform")
        
        let contex = CIContext(options: [kCIContextUseSoftwareRenderer:true])
        
        let outputImage = filter?.outputImage
        let cgImage = contex.createCGImage(outputImage!, from: (outputImage?.extent)!)
        
        let result = UIImage(cgImage: cgImage!)
        return result
    }
    
   public  func formatTimeWork (_ param : [[String : Any]]) -> [String] {
        let temp = NSMutableArray ()
        for obj in param {
            let dayName = obj["dayName"] as! String
            let open = obj["open"] as! String
            let close = obj["close"] as! String
            temp.add("\(String(dayName[...3]))   \(open)  -  \(close)")
        }
        
        return temp as! [String]
    }
    
    func imageCell(_ param : photo?, _ size : CGRect, _ image : UIImage?) -> UIImageView {
        
        let imageView:UIImageView = UIImageView(frame: CGRect.init(origin: size.origin, size: size.size))
        imageView.contentMode = .scaleAspectFit
        
        if param?.url != ""  {
            let manager:SDWebImageManager = SDWebImageManager.shared()
            
            let requestURL:NSURL = URL(string:param!.url)! as NSURL
            manager.loadImage(with: requestURL as URL, options: SDWebImageOptions.highPriority, progress: { (start, progress, url) in
                
            }) { (image, data, error, cached, finished, url) in
                if (error == nil && (image != nil) && finished) {
                    DispatchQueue.main.async {
                        imageView.image =  Toucan(image: image!).resize(size.size,  fitMode: Toucan.Resize.FitMode.crop).image
                    }
                }
            }
        } else {
            imageView.image = image
        }
        return imageView
    }
    
   public func replaceDict(_ param : Any,key : String, value : Any) -> [String : Any] {
        
        var newObj = param as! [String : Any]
        newObj[key] = value
        
        return newObj
    }

   public func contentFomfile() -> Any? {

        if let path = Bundle.main.path(forResource: "foodoo", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
 
                return jsonResult as Any

            } catch {
                 print("An error occurred: \(error)")
            }
        }
        return nil;
    }
    

   public func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}

// MARK: - String


extension String {
    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        get {
            return self[..<index(startIndex, offsetBy: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeThrough<Int>) -> Substring {
        get {
            return self[...index(startIndex, offsetBy: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeFrom<Int>) -> Substring {
        get {
            return self[index(startIndex, offsetBy: value.lowerBound)...]
        }
    }
}

// MARK: - UIView

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        
        get{
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = borderColor?.cgColor
        }
    }
}

