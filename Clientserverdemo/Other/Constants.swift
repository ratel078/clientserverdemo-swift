//
//  Constants.swift
//  Clientserverdemo
//
//  Created by Sergiy Bekker on 16.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

struct Constants {
    
    let BASE_URL : String = "http://api.foodoo.zeta.diffco.us"
    let restaurant_imageUrl = "https://foodoo-dev.s3-us-west-1.amazonaws.com/uploads/places/thumb_274_1920_x_1280.jpeg"
    func baseUrl(_ param:String) -> String
    {
        return "\(BASE_URL)\(param)"
    }
    func app_vesion() -> (String) {
        let nsObject: Any? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as Any
        return nsObject as! String
    }
    func os_platrorm() -> (String) {
        return UIDevice().systemName
    }
    func os_version() -> (String) {
        return UIDevice().systemVersion
    }
    
    
}
