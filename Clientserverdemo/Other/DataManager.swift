//
//  DataManager.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage


class DataManager: NSObject {

    static let sharedInstance = DataManager()
    var queue : OperationQueue
    private var photos = NSMutableArray()
    
    
    // MARK: - WebService method
    
    func foodooContent(completion: @escaping (_ result: Any) -> Void)
    {
        let service = WebService()
        service.requestFoodoo(requestParm().searchParam, completion: { (result: Any) in
            completion(result)
        })
    }
    
    // MARK: - Initialization
    
    override init() {
        queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1;
    }
    
    // MARK: - CoreData Stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "Foodoo", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Foodoo.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            print(error.localizedDescription)
            var dict = [String: Any]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as Any
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as Any
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
  
    
    //MARK: - CoreData  methods
    
    
    func saveItem(_ content : Foodoo){
        
        queue.cancelAllOperations()

        let privateContext = self.privateContext()
        privateContext.perform {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FoodooEntity")
            do {
                let item : FoodooEntity!
                let results = try self.managedObjectContext.fetch(fetchRequest)
                if results.count == 0 {
                    item = NSEntityDescription.insertNewObject(forEntityName: "FoodooEntity",
                                                               into: self.managedObjectContext) as! FoodooEntity
                } else {
                    item = results[0] as! FoodooEntity
                }
                
                item.id = Int16(content.model.id ?? 0)
                item.restaurant_name = content.model.restaurant_name ?? ""
                item.subtitle = content.model.subtitle ?? ""
                item.price_rate = Int16(content.model.price_rate ?? 0)
                item.time_walk = content.model.time_walk ?? ""
                item.time_car = content.model.time_car ?? ""
                item.restaurant_image = content.model.restaurant_image ?? ""
                item.restaurant_address = content.model.restaurant_address ?? ""
                item.phone = content.model.phone ?? ""
                item.website = content.model.website ?? ""
                item.is_open = Int16(content.model.is_open ?? 0)
                item.status = content.model.status ?? ""
                item.minutes_to_close = Int16(content.model.minutes_to_close ?? 0)
                item.internal_rate = Int16(content.model.internal_rate ?? 0)
                item.internal_rate_dynamic = Int16(content.model.internal_rate_dynamic ?? 0)
                item.internal_rate_approval = Int16(content.model.internal_rate ?? 0)
                item.internal_rate = Int16(content.model.internal_rate_approval ?? 0)
                item.internal_rate_count_agree = Int16(content.model.internal_rate ?? 0)
                item.internal_rate = Int16(content.model.internal_rate_count_agree ?? 0)
                item.zagat_rate = Int16(content.model.zagat_rate ?? 0)
                item.yelp_rate = Int16(content.model.yelp_rate ?? 0)
                item.tripadviser_rate = Int16(content.model.tripadviser_rate ?? 0)
                item.zomato_rate = Int16(content.model.zomato_rate ?? 0)
                item.michelin = Int16(content.model.michelin ?? 0)
                item.booking = content.model.booking ?? ""
                item.bookmark = Int16(content.model.bookmark ?? 0)
                item.explore_menu = content.model.explore_menu ?? ""
                item.distance = content.model.distance ?? ""
                item.modified_date = content.model.modified_date ?? ""
                item.food_tags = NSKeyedArchiver.archivedData(withRootObject: content.model.food_tags!) as Data
                item.extra_tags = NSKeyedArchiver.archivedData(withRootObject: content.model.extra_tags!) as Data
                item.place_tags = NSKeyedArchiver.archivedData(withRootObject: content.model.place_tags!) as Data
                item.photos = NSKeyedArchiver.archivedData(withRootObject: content.model.photos!) as Data
                item.working_hours = NSKeyedArchiver.archivedData(withRootObject: content.model.working_hours!) as Data
                item.restaurant_cords = NSKeyedArchiver.archivedData(withRootObject: content.model.restaurant_cords!) as Data
                item.workTime = NSKeyedArchiver.archivedData(withRootObject: content.model.workTime!) as Data
                
                try privateContext.save()
                self.managedObjectContext.performAndWait {
                    self.saveContext()
                }
            } catch {
                print(error)
            }
        
            
        }
        self.loadTmbl(content.model.restaurant_image!)
        
        let photos = NSMutableArray()
        if(content.model.photos!.count > 0){
            for obj in content.model.photos!{
                photos.add(obj.url)
            }
            self.loadPhotos(photos as! [String], content)
        }
        
        
    }
    
    
    func getItem(completion: @escaping (_ result: Foodoo?) -> Void){
        _ = NSFetchRequest<NSFetchRequestResult>(entityName: "FoodooEntity")
        do {
            
            let privateContext = self.privateContext()
            privateContext.perform {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FoodooEntity")
                do {
                    let results = try self.managedObjectContext.fetch(fetchRequest)
                    if results.count > 0 {
                        let item = Foodoo()
                        let obj = results[0] as? NSManagedObject
                        item.model.id = obj?.value(forKey: "id") as? Int
                        item.model.restaurant_name = obj?.value(forKey: "restaurant_name") as? String
                        item.model.subtitle = obj?.value(forKey: "subtitle") as? String
                        item.model.price_rate = obj?.value(forKey: "price_rate") as? Int
                        item.model.time_walk = obj?.value(forKey: "time_walk") as? String
                        item.model.time_car = obj?.value(forKey: "subtitle") as? String
                        item.model.restaurant_image = obj?.value(forKey: "restaurant_image") as? String
                        item.model.restaurant_address = obj?.value(forKey: "restaurant_address") as? String
                        item.model.phone = obj?.value(forKey: "phone") as? String
                        item.model.website = obj?.value(forKey: "website") as? String
                        item.model.is_open = obj?.value(forKey: "is_open") as? Int
                        item.model.status = obj?.value(forKey: "status") as? String
                        item.model.minutes_to_close = obj?.value(forKey: "minutes_to_close") as? Int
                        item.model.internal_rate =  obj?.value(forKey: "internal_rate") as? Int
                        item.model.internal_rate_dynamic = obj?.value(forKey: "internal_rate_dynamic") as? Int
                        item.model.internal_rate_approval = obj?.value(forKey: "internal_rate_approval") as? Int
                        item.model.internal_rate = obj?.value(forKey: "internal_rate") as? Int
                        item.model.internal_rate_count_agree = obj?.value(forKey: "internal_rate_count_agree") as? Int
                        item.model.internal_rate = obj?.value(forKey: "internal_rate") as? Int
                        item.model.zagat_rate = obj?.value(forKey: "zagat_rate") as? Int
                        item.model.yelp_rate = obj?.value(forKey: "yelp_rate") as? Int
                        item.model.tripadviser_rate = obj?.value(forKey: "tripadviser_rate") as? Int
                        item.model.zomato_rate = obj?.value(forKey: "zomato_rate") as? Int
                        item.model.michelin = obj?.value(forKey: "michelin") as? Int
                        item.model.booking = obj?.value(forKey: "booking") as? String
                        item.model.bookmark = obj?.value(forKey: "bookmark") as? Int
                        item.model.explore_menu = obj?.value(forKey: "explore_menu") as? String
                        item.model.distance = obj?.value(forKey: "distance") as? String
                        item.model.modified_date = obj?.value(forKey: "modified_date") as? String
                        if (obj?.value(forKey: "food_tags") as? Data) != nil{
                        item.model.food_tags = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "food_tags") as? Data)!) as? [food_tags]
                        }
                        if (obj?.value(forKey: "extra_tags") as? Data) != nil{
                            item.model.extra_tags = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "extra_tags") as? Data)!) as? [food_tags]
                        }
                        if (obj?.value(forKey: "place_tags") as? Data) != nil{
                            item.model.place_tags = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "place_tags") as? Data)!) as? [food_tags]
                        }
                        if (obj?.value(forKey: "photos") as? Data) != nil{
                            item.model.photos = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "photos") as? Data)!) as? [photo]
                        }
                        if (obj?.value(forKey: "working_hours") as? Data) != nil{
                            item.model.working_hours = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "working_hours") as? Data)!) as? [working_hours]
                        }
                        if (obj?.value(forKey: "restaurant_cords") as? Data) != nil{
                            item.model.restaurant_cords = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "restaurant_cords") as? Data)!) as? restaurant_cords
                        }
                        if (obj?.value(forKey: "restimage") as? Data) != nil{
                            item.model.restimage = UIImage(data: (obj?.value(forKey: "restimage") as? Data)!)
                        }
                        if (obj?.value(forKey: "photosimage") as? Data) != nil{
                            item.model.photosimage = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "photosimage") as? Data)!) as? [UIImage]
                            
                        }
                        if (obj?.value(forKey: "workTime") as? Data) != nil{
                            item.model.workTime = NSKeyedUnarchiver.unarchiveObject(with: (obj?.value(forKey: "workTime") as? Data)!) as? [String]
                            
                        }
              
                        completion(item)
                    } else {
                        completion(nil)
                    }
                    
                } catch {
                    print(error)
                }
                
            }

        }
    }
    
    fileprivate func removeItem(){
        
        let privateContext = self.privateContext()
        privateContext.perform {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FoodooEntity")
            do {
                let results = try self.managedObjectContext.fetch(fetchRequest)
                for obj in results as! [NSManagedObject] {
                    self.managedObjectContext.delete(obj)
                }
                try privateContext.save()
                self.managedObjectContext.performAndWait {
                    self.saveContext()
                }
            } catch {
                print(error)
            }
            
        }
        
    }
    
    //MARK: - Private methods

    fileprivate func updateThmblItem(_ image : UIImage){
        
        let privateContext = self.privateContext()
        privateContext.perform {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FoodooEntity")
            do {
                let results = try self.managedObjectContext.fetch(fetchRequest)
                let data = UIImagePNGRepresentation(image) as NSData?
                let item = results[0] as! FoodooEntity
                item.restimage = data! as Data
              
                try privateContext.save()
                self.managedObjectContext.performAndWait {
                    self.saveContext()
                }
            } catch {
                print(error)
            }
            
        }
        
    }
    
    fileprivate func loadTmbl (_ content:String){

        
        let operation : BlockOperation = BlockOperation(block: {
            let manager:SDWebImageManager = SDWebImageManager.shared()
            let requestURL:NSURL = URL(string:content)! as NSURL
            
            manager.loadImage(with: requestURL as URL, options: SDWebImageOptions.highPriority, progress: { (start, progress, url) in
                
            }) { (image, data, error, cached, finished, url) in
                if (error == nil && (image != nil) && finished) {
                    DispatchQueue.main.async {
                        self.updateThmblItem(image!)
                    }
                }
            }
        })
        
        queue.addOperation(operation);
        
        
        
    }
    
    fileprivate func updatePhotoItems(_ image : UIImage, _ content: Foodoo){

        let privateContext = self.privateContext()
        privateContext.perform {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FoodooEntity")
            do {
                let results = try self.managedObjectContext.fetch(fetchRequest)
                let item = results[0] as! FoodooEntity
                item.photosimage = NSKeyedArchiver.archivedData(withRootObject: self.photos) as Data
                try privateContext.save()
                self.managedObjectContext.performAndWait {
                    self.saveContext()
                }
                self.photos.removeAllObjects()
            } catch {
                print(error)
            }
            
        }
        
    }
    fileprivate func loadPhotos (_ content:[String],_ item: Foodoo){
        
        let operation : BlockOperation = BlockOperation(block: {
            
            for obj in content{
                let manager:SDWebImageManager = SDWebImageManager.shared()
                let requestURL:NSURL = URL(string:obj)! as NSURL
                
                manager.loadImage(with: requestURL as URL, options: SDWebImageOptions.highPriority, progress: { (start, progress, url) in
                    
                }) { (image, data, error, cached, finished, url) in
                    if (error == nil && (image != nil) && finished) {
                        DispatchQueue.main.async {
                            self.photos.add(image ?? UIImage())
                            if self.photos.count == item.model.photos?.count{
                                //self.updatePhotoItems(image!,item)
                            }
                        }
                    }
                }
            }
        })

        queue.addOperation(operation);
        
        
        
    }
    
    
    
    fileprivate func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    fileprivate func privateContext () -> NSManagedObjectContext{
        
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.persistentStoreCoordinator = managedObjectContext.persistentStoreCoordinator
        
        return privateContext
    }
}
