//
//  WebService.swift
//  DiffcoTest
//
//  Created by Sergiy Bekker on 06.11.17.
//  Copyright © 2017 SBApps. All rights reserved.
//

import UIKit

enum appendUrl : String {
    case SEARCHURL = "/v1/search/list"
}

enum requestTYPE : String {
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
}
struct requestParm {
    
    var searchParam: [String : Any] {
        get{
            return ["order": "[\"distance\",\"ASC\"]",
                    "limit": 20,
                    "page": 1]
        }
    }
}

class WebService: NSObject {

    var completionBlock : ((Any) -> Void)?
    
    
    func requestFoodoo(_ param:[String : Any], completion: @escaping (_ result: Any) -> Void) {
        completionBlock = completion
        self.data_request(param, requestTYPE.POST)
    }
    
    fileprivate func configContent(_ obj: inout [String : Any]) -> [String : Any] {
        var param = obj as [String : Any]
        param = self.replaceDict(param, key: "restaurant_image", value: Constants().restaurant_imageUrl)
        let temp =  NSMutableArray()
        for i in 0..<10 {
            if i % 2 == 0 {
                temp.add(["url":"https://foodoo-dev.s3-us-west-1.amazonaws.com/uploads/places/thumb_274_1920_x_1280.jpeg"])
            } else{
                temp.add(["url":"https://foodoo-dev.s3-us-west-1.amazonaws.com/uploads/places/thumb_275_1920_x_1280.jpeg"])
            }
        }
        param = self.replaceDict(param, key: "photos", value:temp)
        return param
        
        
    }
    
    fileprivate func data_request(_ param:[String : Any], _ type:requestTYPE) {

        let requestURL:NSURL = URL(string:Constants().baseUrl(appendUrl.SEARCHURL.rawValue))! as NSURL
        var request = URLRequest(url: requestURL as URL)

        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(Constants().app_vesion(), forHTTPHeaderField: "APP_VERSION")
        request.setValue(Constants().os_platrorm(), forHTTPHeaderField: "OS_PLATFORM")
        request.setValue(Constants().os_version(), forHTTPHeaderField: "OS_VERSION")
  
        request.httpMethod = type.rawValue
        if(type.rawValue == requestTYPE.POST.rawValue || type.rawValue == requestTYPE.PUT.rawValue){
            do{
                let data = try JSONSerialization.data(withJSONObject: param, options:JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody =  data
            } catch{
                print(error)
            }
            
        }
        
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in

            if (error != nil) {
                print(error ?? "")
            }
            else {
                do{
                    if let responseObj = try JSONSerialization.jsonObject(with: (data)!, options: .allowFragments) as? NSDictionary{
                        
                        if JSONSerialization.isValidJSONObject(responseObj){
                            if let dictionary = responseObj as? [String: Any] {
                                
                                let status = dictionary["status"] as! String
                      
                                var response = responseObj as! [String: Any]
                                if status == "error" {
                                    response = self.contentFomfile() as! [String : Any]
                                }
                                let result = response["result"] as! [[String: Any]]
                                var obj = result[0] as [String: Any]
                                obj = self.configContent(&obj)
                                
                                self.completionBlock!(obj as Any)
                            }
                           
                           
                        }
                        else{
                            if let jsonString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) {
                                print("JSON: \n\n \(jsonString)")
                            }
                            fatalError("Can't parse JSON \(String(describing: error))")
                        }
                    }
                    
                }
                catch let error as NSError {
                    print("An error occurred: \(error)")
                   
                    let response = self.contentFomfile() as! [String : Any]
                    let result = response["result"] as! [[String: Any]]
                    var obj = result[0] as [String: Any]
                    obj = self.replaceDict(obj, key: "restaurant_image", value: Constants().restaurant_imageUrl)
                    obj = self.replaceDict(obj, key: "photos", value: [["url":"https://foodoo-dev.s3-us-west-1.amazonaws.com/uploads/places/thumb_274_1920_x_1280.jpeg"],["url":"https://foodoo-dev.s3-us-west-1.amazonaws.com/uploads/places/thumb_275_1920_x_1280.jpeg"]])
                    
                    self.completionBlock!(obj as Any)
                }
            }
        })
        task.resume()
    }
}
