//
//  FoodooEntity+CoreDataProperties.swift
//  
//
//  Created by Sergiy Bekker on 09.11.17.
//
//

import Foundation
import CoreData


extension FoodooEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FoodooEntity> {
        return NSFetchRequest<FoodooEntity>(entityName: "FoodooEntity")
    }

    @NSManaged public var booking: String?
    @NSManaged public var bookmark: Int16
    @NSManaged public var distance: String?
    @NSManaged public var explore_menu: String?
    @NSManaged public var extra_tags: NSData?
    @NSManaged public var food_tags: NSData?
    @NSManaged public var id: Int16
    @NSManaged public var internal_rate: Int16
    @NSManaged public var internal_rate_approval: Int16
    @NSManaged public var internal_rate_count_agree: Int16
    @NSManaged public var internal_rate_dynamic: Int16
    @NSManaged public var is_open: Int16
    @NSManaged public var michelin: Int16
    @NSManaged public var minutes_to_close: Int16
    @NSManaged public var modified_date: String?
    @NSManaged public var phone: String?
    @NSManaged public var photos: NSData?
    @NSManaged public var photosimage: NSData?
    @NSManaged public var place_tags: NSData?
    @NSManaged public var price_rate: Int16
    @NSManaged public var restaurant_address: String?
    @NSManaged public var restaurant_cords: NSData?
    @NSManaged public var restaurant_image: String?
    @NSManaged public var restaurant_name: String?
    @NSManaged public var restimage: NSData?
    @NSManaged public var status: String?
    @NSManaged public var subtitle: String?
    @NSManaged public var time_car: String?
    @NSManaged public var time_walk: String?
    @NSManaged public var tripadviser_rate: Int16
    @NSManaged public var website: String?
    @NSManaged public var working_hours: NSData?
    @NSManaged public var yelp_rate: Int16
    @NSManaged public var zagat_rate: Int16
    @NSManaged public var zomato_rate: Int16
    @NSManaged public var workTime: NSData?

}
